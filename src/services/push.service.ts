import { TokenService } from '@ons/ons-mobile-login';
import { Injectable } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Firebase } from '@ionic-native/firebase';
import { tap } from 'rxjs/operators';
import { Config } from '../environment/config';

@Injectable()
export class PushService {

  private _appV: any
  constructor(
    public httpClient: HttpClient,
    public appVersion: AppVersion,
    public alertCtrl: AlertController,
    public device: Device,
    public fcmSrv: Firebase,
    public plt: Platform,
    public tokenSrv: TokenService
  ) {

    if (this.plt.is('cordova')) {
      this.appVersion.getVersionNumber().then((x: string) => {
        this.appV = x;
      })
      this.ativarPushListener()
    } else {
        this.alertCtrl.create({
          title: 'ERRO DE EXECUÇÃO',
          subTitle: 'As mensagens de push não estarão disponiveis no browser. Somente nos devices',
          buttons: ['OK']
        }).present()
      }
    
  }


  listenToNotifications() {
    return this.fcmSrv.onNotificationOpen()
  }
  ativarPushListener() {
    this.fcmSrv.onNotificationOpen().pipe(
      tap((msg) => {
        this.showPushAlert(msg.TipoMSG)
      })).subscribe((data) => {
        console.log('o q estou recebendo de fato?', data)
      })
  };
  public set appV(data) {
    this._appV = data
  }

  public get appV() {
    return this._appV
  }

  async registrarFirebase() {
    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + this.tokenSrv.getToken().access_token
      }
    );
    const options = { headers };
    const body = {
      "appVersao": this.appV,
      "dispositivoId": await this.fcmSrv.getToken(),//this.device.uuid,
      "dispositivoDetalhe": {
        "so": this.device.platform,
        "modelo": this.device.model
      },
      "topicos": [
        "Publicações"
      ]
    };

    console.log('O que temos dentro do Body', body);
    console.log('Vindo dentro do options', options);



    const jsonBody = JSON.stringify(body)
    this.httpClient.post(Config.URL_REGISTRAR_PUSH, jsonBody, options)
      .subscribe(data => {
        console.log('Resposta de serviço de registro', data);
        return data;
      });
  }


  showPushAlert(msgType?) {
    const msg = this.alertCtrl.create({
      title: 'FELIZ ANIVERSARIO!',
      subTitle: 'O Ons lhe deseja um feliz aniversario e um excelente dia'
    });
    msg.present();
    /* switch (msgType) {
      case 'ANIVERSARIANTE':
        const aniversario = this.alertCtrl.create({
          title: 'FELIZ ANIVERSARIO!',
          subTitle: 'O Ons lhe deseja um feliz aniversario e um excelente dia'
        });
        aniversario.present();
        break;
      case 'CORPORATIVA':
        const corporativa = this.alertCtrl.create({
          title: 'Items de reunião',
          subTitle: 'body da mensagem corporativa'
        });
        corporativa.present();
        break;
      default:
        break;
    } */


  }
}
