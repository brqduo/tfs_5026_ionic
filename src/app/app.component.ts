import { Component } from '@angular/core';
import { Platform, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginService, ErrorService, EnvironmentService, NetWorkService } from '@ons/ons-mobile-login';
import { LoginPage, ImageService, UtilService } from '@ons/ons-mobile-login';
import { ContatosPage } from '../pages/contatos/contatos';

import { tap } from 'rxjs/operators';
import { PushService } from '../services/push.service';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;
  Nome: string;
  constructor(platform: Platform,
    statusBar: StatusBar,
    public loginSrv: LoginService,
    splashScreen: SplashScreen,
    public imageSrv: ImageService,
    public envSrv: EnvironmentService,
    public utilSrv: UtilService,
    public errorSrv: ErrorService,
    public networkSrv: NetWorkService,
    private pushSrv: PushService,
    public alertCtrl: AlertController,
    public app: App) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      imageSrv.SetDefault();
      loginSrv.setAplicationName('Mobile.Contatos');
      envSrv.setEnv('DSV');
      this.rootPage = LoginPage;
      this.loginSrv.onConectedChange
        .subscribe((u: any) => {
          if (u !== undefined) {
            if (u.Connected) {
              this.Nome = u.User_Full_Name
              console.log('Retorno de Login_onConectedChange', u);
              this.pushSrv.registrarFirebase();
              this.app.getActiveNav().setRoot(ContatosPage);
            }
          }
        })
    });
  }
}

